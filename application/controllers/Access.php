<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Access extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function login()
	{
        $this->session->sess_destroy();
		$this->load->view('access/login');
	}

	public function oAuth(){
		if ($_GET['token'] != null){
			// set session
			$this->session->set_userdata(
				array(
					'TOKEN' => $_GET['token'],
					'FIRST_NAME' => $_GET['first_name'],
					'MIDDLE_NAME' => $_GET['middle_name'],
					'LAST_NAME' => $_GET['last_name'],
					'FULL_NAME' => $_GET['full_name'],
					'EMAIL' => $_GET['email'],
				)
				);

			// redirect admin dashboard 
			redirect(base_url('SystemSetup/dashboard'));
		}
	}
}