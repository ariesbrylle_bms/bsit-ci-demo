<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">

                <li>
                    <a href="<?php echo base_url('SystemSetup/dashboard')?>" class="waves-effect">
                        <i class="bx bxs-dashboard"></i>
                        <span key="t-calendar">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('SystemSetup/user')?>" class="waves-effect">
                        <i class="bx bx-user"></i>
                        <span key="t-calendar">Student</span>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('SystemSetup/program')?>" class="waves-effect">
                        <i class="bx bx-columns"></i>
                        <span key="t-calendar">Program</span>
                    </a>
                </li>



            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
<div class="main-content">