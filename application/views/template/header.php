<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>SAS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url('assets')?>/images/favicon.ico" />

    <link href="<?php echo base_url('assets')?>/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets')?>/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css"
        rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets')?>/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css"
        rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url('assets')?>/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet"
        type="text/css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets')?>/libs/toastr/build/toastr.min.css">

    <!-- Responsive datatable examples -->
    <link href="<?php echo base_url('assets')?>/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css"
        rel="stylesheet" type="text/css" />

    <!-- Bootstrap Css -->
    <link href="<?php echo base_url('assets')?>/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet"
        type="text/css" />
    <!-- Icons Css -->
    <link href="<?php echo base_url('assets')?>/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?php echo base_url('assets')?>/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url('assets')?>/libs/jquery/jquery.min.js"></script>

    <script src="<?php echo base_url('assets')?>/js/common.js"></script>


    <script>
    var base_url = '<?php echo base_url() ?>';
    </script>
</head>

<body data-sidebar="dark">

    <!-- <body data-layout="horizontal" data-topbar="dark"> -->

    <!-- Begin page -->
    <div id="layout-wrapper">
        <header id="page-topbar">
            <div class="navbar-header">
                <div class="d-flex">
                    <!-- LOGO -->
                    <div class="navbar-brand-box">
                        <a href="index.html" class="logo logo-dark">
                            <span class="logo-sm">
                                <img src="<?php echo base_url('assets')?>/images/logo.svg" alt="" height="22" />
                            </span>
                            <span class="logo-lg">
                                <img src="<?php echo base_url('assets')?>/images/logo-dark.png" alt="" height="17" />
                            </span>
                        </a>

                        <a href="index.html" class="logo logo-light">
                            <span class="logo-sm">
                                <img src="<?php echo base_url('assets')?>/images/logo-light.svg" alt="" height="22" />
                            </span>
                            <span class="logo-lg">
                                <img src="<?php echo base_url('assets')?>/images/logo-light.png" alt="" height="19" />
                            </span>
                        </a>
                    </div>

                    <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect"
                        id="vertical-menu-btn">
                        <i class="fa fa-fw fa-bars"></i>
                    </button>
                </div>

                <div class="d-flex">

                    <div class="dropdown d-none d-lg-inline-block ms-1">
                        <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen">
                            <i class="bx bx-fullscreen"></i>
                        </button>
                    </div>


                    <div class="dropdown d-inline-block">
                        <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="rounded-circle header-profile-user"
                                src="<?php echo base_url('assets')?>/images/users/avatar-1.jpg" alt="Header Avatar" />
                            <span class="d-none d-xl-inline-block ms-1"
                                key="t-henry"><?php echo $this->session->userdata('FIRST_NAME')?></span>
                            <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-end">
                            <!-- item-->
                            <a class="dropdown-item" href="#"><i class="bx bx-user font-size-16 align-middle me-1"></i>
                                <span key="t-profile">Profile</span></a>

                            <a class="dropdown-item text-danger" href="../Access/login"><i
                                    class="bx bx-power-off font-size-16 align-middle me-1 text-danger"></i>
                                <span key="t-logout">Logout</span></a>
                        </div>
                    </div>


                </div>
            </div>
        </header>