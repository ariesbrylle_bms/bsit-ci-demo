<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <script>
                document.write(new Date().getFullYear());
                </script>
                © <?php echo SYSTEM_NAME;?>
            </div>

        </div>
    </div>
</footer>
</div>
<!-- end main content-->
</div>
<!-- END layout-wrapper -->

<!-- JAVASCRIPT -->

<script src="<?php echo base_url('assets')?>/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url('assets')?>/libs/metismenu/metisMenu.min.js"></script>
<script src="<?php echo base_url('assets')?>/libs/simplebar/simplebar.min.js"></script>
<script src="<?php echo base_url('assets')?>/libs/node-waves/waves.min.js"></script>
<script src="<?php echo base_url('assets')?>/libs/parsleyjs/parsley.min.js"></script>
<script src="<?php echo base_url('assets')?>/libs/toastr/build/toastr.min.js"></script>

<script src="<?php echo base_url('assets')?>/libs/select2/js/select2.min.js"></script>
<script src="<?php echo base_url('assets')?>/libs/sweetalert2/sweetalert2.min.js"></script>

<script src="<?php echo base_url('assets')?>/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets')?>/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<script src="<?php echo base_url('assets')?>/js/app.js"></script>
</body>

</html>