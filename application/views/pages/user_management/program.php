<div class="page-content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-0 font-size-18">Program</h4>
                    <div class="page-title-right">
                        <button type="button" class="btn btn-success waves-effect waves-light" id="btn_add"
                            onClick="return formReset('show')">
                            <i class="bx bx-plus font-size-16 align-middle"></i>New
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- form -->
        <div class="row" id="div_form">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Program Form</h4>

                        <form id="form_id" name="form_id" enctype="multipart/form-data">
                            <input type="hidden" name="uuid" id="uuid" value="" />

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3"><label class="form-label">Program Code</label>
                                        <input type="text" data-parsley-required="true" data-parsley-maxlength="25"
                                            class="form-control" id="program_code" name="program_code">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3"><label class="form-label">Program Name</label>
                                        <input type="text" data-parsley-required="true" data-parsley-maxlength="100"
                                            class="form-control" id="program_name" name="program_name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-3"><label class="form-label">Program Description</label>
                                        <textarea required class="form-control" rows="3" name="program_description"
                                            id="program_description"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <button type="reset" class="btn btn-secondary waves-effect"
                                    onClick="return formReset('hide')">Cancel</button>
                                <button type="submit" class="btn btn-primary w-md submit">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- end card body -->
                </div>
                <!-- end card -->
            </div>
            <!-- end col -->
        </div>
        <!-- end form -->

        <!-- data table -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <h4 class="card-title">List of Programs</h4>


                        <table id="data-table"
                            class="table table-bordered dt-responsive wrap w-100 dataTable no-footer dtr-inline">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
        <!-- end data table -->
    </div>
    <!-- container-fluid -->
</div>
<!-- End Page-content -->

<script src="<?php echo base_url('assets')?>/js/user_management/program.js"></script>