<div class="page-content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-0 font-size-18">User</h4>
                    <div class="page-title-right">
                        <button type="button" class="btn btn-success waves-effect waves-light" id="btn_add"
                            onClick="return formReset('show')">
                            <i class="bx bx-plus font-size-16 align-middle"></i>New
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- form -->
        <div class="row" id="div_form">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Student Form</h4>

                        <form id="form_id" name="form_id" enctype="multipart/form-data">
                            <input type="hidden" name="uuid" id="uuid" value="" />
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-3">
                                        <img src="https://avatars.dicebear.com/api/bottts/smile.svg" alt=""
                                            class="rounded avatar-lg img-thumbnail" style="object-fit: cover;"
                                            id="photo_path_placeholder" name="photo_path_placeholder">

                                    </div>
                                    <div class="mb-3">
                                        <label for="photo_path" class="form-label">Profile Picture</label>
                                        <input class="form-control" type="file" id="photo_path" name="photo_path"
                                            accept="image/*">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="mb-3"><label class="form-label">Salutation</label>
                                        <select class="form-control select2-search-disable" data-parsley-required="true"
                                            style="width:100%" id="salutation" name="salutation">
                                            <option value="Mr.">Mr.</option>
                                            <option value="Ms.">Ms.</option>
                                            <option value="Mrs.">Mrs.</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="mb-3"><label class="form-label">First Name</label>
                                        <input type="text" data-parsley-required="true" class="form-control"
                                            id="first_name" name="first_name">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="mb-3"><label class="form-label">Middle Name</label>
                                        <input type="text" class="form-control" id="middle_name" name="middle_name">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="mb-3"><label class="form-label">Last Name</label>
                                        <input type="text" data-parsley-required="true" class="form-control"
                                            id="last_name" name="last_name">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="mb-3"><label class="form-label">Name Extension</label>
                                        <input type="text" class="form-control" id="name_suffix" name="name_suffix">
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-3"><label class="form-label">ID Number</label>
                                        <input type="text" data-parsley-required="true" class="form-control"
                                            id="id_number" name="id_number">
                                        <input type="hidden" class="form-control" value="Student" id="student_faculty"
                                            name="student_faculty">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-3"><label class="form-label">Program</label>
                                        <select class="form-control select2" id="program_id"
                                            data-parsley-required="true" name="program_id" style="width:100%"
                                            onchange="return loadSectionByProgram(this.value)">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-3"><label class="form-label">Section</label>
                                        <select class="form-control select2" id="section_id"
                                            data-parsley-required="true" name="section_id" style="width:100%">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-3"><label class="form-label">Gender</label>
                                        <select class="form-control select2-search-disable" data-parsley-required="true"
                                            style="width:100%" id="gender" name="gender">
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="mb-3"><label class="form-label">Mobile Number (917XXXXXXX)</label>
                                        <input type="text" data-parsley-required="true" class="form-control"
                                            id="contact_number" name="contact_number">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-3"><label class="form-label">Email Address</label>
                                        <input type="text" data-parsley-required="true" parsley-type="email"
                                            class="form-control" id="email" name="email">
                                    </div>
                                </div>

                            </div>

                            <div>
                                <button type="reset" class="btn btn-secondary waves-effect"
                                    onClick="return formReset('hide')">Cancel</button>
                                <button type="submit" class="btn btn-primary w-md submit">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- end card body -->
                </div>
                <!-- end card -->
            </div>
            <!-- end col -->
        </div>
        <!-- end form -->

        <!-- data table -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <h4 class="card-title">List of Users</h4>


                        <table id="data-table"
                            class="table table-bordered dt-responsive wrap w-100 dataTable no-footer dtr-inline">
                            <thead>
                                <tr>
                                    <th>Student #</th>
                                    <th>Name</th>
                                    <th>Section</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
        <!-- end data table -->
    </div>
    <!-- container-fluid -->
</div>
<!-- End Page-content -->

<script src="<?php echo base_url('assets')?>/js/user_management/user.js"></script>