$(function () {
	// function that will be called once the file is loaded.
	// $("#username").val(); // access value using id
	// $("input[name=username]").val(); // access value using name

	$("#form_id").on("submit", function (e) {
		e.preventDefault(); // prevent page refresh

		if ($("#form_id").parsley().validate()) {
			// no validation error
			$.ajax({
				url: apiURL + "login",
				type: "POST", // post, put, delete, get
				data: {
					email: $("#username").val(),
					password: $("#password").val(),
				},
				dataType: "json",
				success: function (data) {
					localStorage.setItem("TOKEN", data.data.token);
					console.log(data);

					let session_data = "";

					session_data += "token=" + data.data.token;
					session_data += "&first_name=" + data.data.user_details[0].first_name;
					session_data +=
						"&middle_name=" + data.data.user_details[0].middle_name;
					session_data += "&last_name=" + data.data.user_details[0].last_name;
					session_data += "&full_name=" + data.data.user_details[0].full_name;
					session_data += "&email=" + data.data.user_details[0].email;

					window.location.replace(baseURL + "Access/oAuth?" + session_data);
				},
				error: function ({ responseJSON }) {
					console.log(responseJSON);
					notification("error", "", responseJSON.message.join());
				},
			});
		}
	});
});
