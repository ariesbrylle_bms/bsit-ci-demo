$(function () {
	formReset("hide");
	// initialize form validation
	$("#form_id").parsley();

	// initialized select2
	$(".select2").select2();

	// load datatable
	loadTable();

	// function to save/update record
	$("#form_id").on("submit", function (e) {
		e.preventDefault();
		trimInputFields();

		if ($("#form_id").parsley().validate()) {
			var form_data = new FormData(this);

			console.log(form_data);

			if ($("#uuid").val() == "") {
				// add record
				form_data.append("password", "P@ssw0rd");
				form_data.append("c_password", "P@ssw0rd");

				$.ajax({
					url: apiURL + "user",
					type: "POST",
					data: form_data,
					dataType: "json",
					contentType: false,
					processData: false,
					cache: false,
					success: function (data) {
						if (data.success) {
							notification("success", "Success!", data.message);
							formReset("hide");
							$("#photo_path_placeholder").attr(
								"src",
								"https://avatars.dicebear.com/api/bottts/smile.svg"
							);
							loadTable();
						} else {
							notification("error", "Error!", data.message);
						}
					},
					error: function ({ responseJSON }) {},
				});
			} else {
				// edit record
				form_data.append("_method", "PUT");

				$.ajax({
					url: apiURL + "user/" + $("#uuid").val(),
					type: "POST",
					data: form_data,
					dataType: "json",
					contentType: false,
					processData: false,
					cache: false,
					success: function (data) {
						if (data.success) {
							notification("success", "Success!", data.message);
							formReset("hide");
							loadTable();
							$("#photo_path_placeholder").attr(
								"src",
								"https://avatars.dicebear.com/api/bottts/smile.svg"
							);
						} else {
							notification("error", "Error!", data.message);
						}
					},
					error: function ({ responseJSON }) {},
				});
			}
		}
	});

	$("#photo_path").change(function () {
		readURL(this);
	});
});

// function to load programs
loadPrograms = () => {
	$.ajax({
		url: apiURL + "program/list",
		type: "GET",
		dataType: "json",
		success: function (responseData) {
			if (responseData.success) {
				$("#program_id").empty();
				$.each(responseData.data, function (i, dataOptions) {
					var options = "";

					options =
						"<option value='" +
						dataOptions.id +
						"'>" +
						dataOptions.program_code +
						" - " +
						dataOptions.program_name +
						"</option>";

					$("#program_id").append(options);
				});
			} else {
				notification("error", "Error!", responseData.message);
			}
		},
		error: function ({ responseJSON }) {},
	});
};

loadPrograms();

// function to load section depending on the selected program
loadSectionByProgram = (program_id) => {
	if (program_id === "") {
		$("#section_id").empty();
		return false;
	}

	$.ajax({
		url: apiURL + "section/" + program_id + "/program",
		type: "GET",
		dataType: "json",
		success: function (responseData) {
			if (responseData.success) {
				$("#section_id").empty();
				$.each(responseData.data, function (i, dataOptions) {
					var options = "";

					options =
						"<option value='" +
						dataOptions.id +
						"'>" +
						dataOptions.section_code +
						"</option>";

					$("#section_id").append(options);
				});
			} else {
				notification("error", "Error!", responseData.message);
			}
		},
		error: function ({ responseJSON }) {},
	});
};

readURL = (input) => {
	var url = input.value;
	var ext = url.substring(url.lastIndexOf(".") + 1).toLowerCase();
	if (
		input.files &&
		input.files[0] &&
		(ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")
	) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$("#photo_path_placeholder").attr("src", e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	} else {
		//$("#img").attr("src", "/assets/no_preview.png");
	}
};

loadTable = () => {
	$.ajaxSetup({
		headers: {
			Accept: "application/json",
			Authorization: "Bearer " + token,
			ContentType: "application/x-www-form-urlencoded",
		},
	});

	$("#data-table").dataTable().fnClearTable();
	$("#data-table").dataTable().fnDraw();
	$("#data-table").dataTable().fnDestroy();
	$("#data-table").dataTable({
		responsive: true,
		serverSide: true,
		order: [[0, "asc"]],
		aLengthMenu: [5, 10, 20, 30, 50, 100],
		aoColumns: [
			{ sClass: "text-left" },
			{ sClass: "text-left" },
			{ sClass: "text-left" },
			{ sClass: "text-center" },
		],
		columns: [
			{
				data: "id_number",
				name: "id_number",
				searchable: true,
				width: "20%",
				className: "dtr-control",
			},
			{
				data: "name",
				name: "name",
				searchable: true,
				width: "30%",
				className: "dtr-control",
			},

			{
				data: "section_id_r.resource.section_code",
				name: "section_id_r.resource.section_code",
				searchable: true,
				width: "20%",
			},
			{
				data: null,
				width: "20%",
				render: function (aData, type, row) {
					let buttons = "";
					// info
					buttons +=
						'<button type="button" onClick="return editData(\'' +
						aData["id"] +
						'\',0)" class="btn btn-light waves-effect"><i class="bx bx-info-circle font-size-16 align-middle"></i></button> ';
					// edit
					buttons +=
						'<button type="button" onClick="return editData(\'' +
						aData["id"] +
						'\',1)" class="btn btn-success waves-effect"><i class="bx bx-edit font-size-16 align-middle"></i></button> ';
					// delete
					buttons +=
						'<button type="button" onClick="return deleteData(\'' +
						aData["id"] +
						'\')" class="btn btn-danger waves-effect"><i class="bx bx-trash font-size-16 align-middle"></i></button> ';
					return buttons; // same class in i element removed it from a element
				},
			},
		],
		ajax: {
			url: apiURL + "user/student",
			type: "GET",
			ContentType: "application/x-www-form-urlencoded",
		},
		fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			let buttons = "";
			// info
			buttons +=
				'<button type="button" onClick="return editData(\'' +
				aData["id"] +
				'\',0)" class="btn btn-light waves-effect"><i class="bx bx-info-circle font-size-16 align-middle"></i></button> ';
			// edit
			buttons +=
				'<button type="button" onClick="return editData(\'' +
				aData["id"] +
				'\',1)" class="btn btn-success waves-effect"><i class="bx bx-edit font-size-16 align-middle"></i></button> ';

			//if (aData["status"] == "Active") {
			// delete
			buttons +=
				'<button type="button" onClick="return deleteData(\'' +
				aData["id"] +
				'\')" class="btn btn-danger waves-effect"><i class="bx bx-trash font-size-16 align-middle"></i></button> ';
			//}

			$("td:eq(0)", nRow).html(aData["id_number"]);
			$("td:eq(1)", nRow).html(aData["name"]);
			$("td:eq(2)", nRow).html(
				aData["section_id_r"].resource != null
					? aData["section_id_r"].resource.section_code
					: ""
			);
			$("td:eq(3)", nRow).html(buttons);
		},
		drawCallback: function (settings) {
			// $("#data-table").removeClass("dataTable");
		},
	});
};

// function to show details for viewing/updating
editData = (id, type) => {
	$.ajax({
		url: apiURL + "user/" + id,
		type: "GET",
		dataType: "json",
		success: function (data) {
			if (data.success) {
				formReset("show");
				$("#uuid").val(data.data["id"]);
				$("#photo_path_placeholder").attr("src", data.data["photo_path"]);
				$("#salutation").val(data.data["salutation"]);
				$("#first_name").val(data.data["first_name"]);
				$("#middle_name").val(data.data["middle_name"]);
				$("#last_name").val(data.data["last_name"]);
				$("#name_suffix").val(data.data["name_suffix"]);
				$("#id_number").val(data.data["id_number"]);
				$("#program_id").val(data.data["program_id"]).trigger("change");
				$("#gender").val(data.data["gender"]).trigger("change");
				$("#contact_number").val(data.data["contact_number"]);
				$("#email").val(data.data["email"]);

				setTimeout(() => {
					$("#section_id").val(data.data["section_id"]).trigger("change");
				}, 1500);

				// if data is for viewing only
				if (type == 0) {
					$("#form_id input, select, textarea").prop("disabled", true);
					$("#form_id button").prop("disabled", false);
					$(".submit").hide();
				}
			} else {
				notification("error", "Error!", data.message);
			}
		},
		error: function (data) {},
	});
};

// function to delete data
deleteData = (id) => {
	Swal.fire({
		title: "Are you sure you want to delete this record?",
		text: "You won't be able to revert this!",
		icon: "warning",
		showCancelButton: !0,
		confirmButtonColor: "#34c38f",
		cancelButtonColor: "#f46a6a",
		confirmButtonText: "Yes, delete it!",
	}).then(function (t) {
		// if user clickes yes, delete it.
		if (t.value) {
			$.ajax({
				url: apiURL + "user/" + id,
				type: "DELETE",
				dataType: "json",
				success: function (data) {
					if (data.success) {
						notification("success", "Success!", data.message);
						loadTable();
					} else {
						notification("error", "Error!", data.message);
					}
				},
				error: function ({ responseJSON }) {},
			});
		}
	});
};
