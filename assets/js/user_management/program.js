$(function () {
	formReset("hide");
	// initialize form validation
	$("#form_id").parsley();

	// load datatable
	loadTable();

	// function to save/update record
	$("#form_id").on("submit", function (e) {
		e.preventDefault();
		trimInputFields();

		if ($("#form_id").parsley().validate()) {
			let form_data = { data: $("#form_id").formToJson() };
			console.log(form_data);
			// $("#form_id").formToJson();
			if ($("#uuid").val() == "") {
				$.ajax({
					url: apiURL + "program",
					type: "POST",
					data: form_data,
					// {
					// 	program_code: $("#program_code").val(),
					// 	program_name: $("#program_name").val(),
					// 	program_description: $("#program_description").val(),
					// },
					dataType: "json",
					success: function (data) {
						if (data.success) {
							notification("success", "Success!", data.message);
							formReset("hide");
							loadTable();
						} else {
							notification("error", "Error!", data.message);
						}
					},
					error: function ({ responseJSON }) {},
				});
			} else {
				// edit record

				$.ajax({
					url: apiURL + "program/" + $("#uuid").val(),
					type: "PUT",
					data: form_data,
					dataType: "json",

					success: function (data) {
						if (data.success) {
							notification("success", "Success!", data.message);
							formReset("hide");
							loadTable();
						} else {
							notification("error", "Error!", data.message);
						}
					},
					error: function ({ responseJSON }) {},
				});
			}
		}
	});
});

loadTable = () => {
	$.ajaxSetup({
		headers: {
			Accept: "application/json",
			Authorization: "Bearer " + token,
			ContentType: "application/x-www-form-urlencoded",
		},
	});

	$("#data-table").dataTable().fnClearTable();
	$("#data-table").dataTable().fnDraw();
	$("#data-table").dataTable().fnDestroy();
	$("#data-table").dataTable({
		responsive: true,
		serverSide: true,
		order: [[0, "asc"]],
		aLengthMenu: [5, 10, 20, 30, 50, 100],
		aoColumns: [
			{ sClass: "text-left" },
			{ sClass: "text-left" },
			{ sClass: "text-left" },
			{ sClass: "text-center" },
		],
		columns: [
			{
				data: "program_code",
				name: "program_code",
				searchable: true,
				width: "20%",
				className: "dtr-control",
			},
			{
				data: "program_name",
				name: "program_name",
				searchable: true,
				width: "30%",
				className: "dtr-control",
			},

			{
				data: "program_description",
				name: "program_description",
				searchable: true,
				width: "20%",
			},
			{
				data: null,
				width: "20%",
				render: function (aData, type, row) {
					let buttons = "";
					// info
					buttons +=
						'<button type="button" onClick="return editData(\'' +
						aData["id"] +
						'\',0)" class="btn btn-light waves-effect"><i class="bx bx-info-circle font-size-16 align-middle"></i></button> ';
					// edit
					buttons +=
						'<button type="button" onClick="return editData(\'' +
						aData["id"] +
						'\',1)" class="btn btn-success waves-effect"><i class="bx bx-edit font-size-16 align-middle"></i></button> ';
					// delete
					buttons +=
						'<button type="button" onClick="return deleteData(\'' +
						aData["id"] +
						'\')" class="btn btn-danger waves-effect"><i class="bx bx-trash font-size-16 align-middle"></i></button> ';
					return buttons; // same class in i element removed it from a element
				},
			},
		],
		ajax: {
			url: apiURL + "program",
			type: "GET",
			ContentType: "application/x-www-form-urlencoded",
		},
		fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			let buttons = "";
			// info
			buttons +=
				'<button type="button" onClick="return editData(\'' +
				aData["id"] +
				'\',0)" class="btn btn-light waves-effect"><i class="bx bx-info-circle font-size-16 align-middle"></i></button> ';
			// edit
			buttons +=
				'<button type="button" onClick="return editData(\'' +
				aData["id"] +
				'\',1)" class="btn btn-success waves-effect"><i class="bx bx-edit font-size-16 align-middle"></i></button> ';

			//if (aData["status"] == "Active") {
			// delete
			buttons +=
				'<button type="button" onClick="return deleteData(\'' +
				aData["id"] +
				'\')" class="btn btn-danger waves-effect"><i class="bx bx-trash font-size-16 align-middle"></i></button> ';
			//}

			$("td:eq(0)", nRow).html(aData["program_code"]);
			$("td:eq(1)", nRow).html(aData["program_name"]);
			$("td:eq(2)", nRow).html(aData["program_description"]);
			$("td:eq(3)", nRow).html(buttons);
		},
		drawCallback: function (settings) {
			// $("#data-table").removeClass("dataTable");
		},
	});
};

// function to show details for viewing/updating
editData = (id, type) => {
	$.ajax({
		url: apiURL + "program/" + id,
		type: "GET",
		dataType: "json",
		success: function (data) {
			if (data.success) {
				formReset("show");
				$("#uuid").val(data.data["id"]);
				$("#program_code").val(data.data["program_code"]);
				$("#program_description").val(data.data["program_description"]);
				$("#program_name").val(data.data["program_name"]);

				// if data is for viewing only
				if (type == 0) {
					$("#form_id input, select, textarea").prop("disabled", true);
					$("#form_id button").prop("disabled", false);
					$(".submit").hide();
				}
			} else {
				notification("error", "Error!", data.message);
			}
		},
		error: function (data) {},
	});
};

// function to delete data
deleteData = (id) => {
	Swal.fire({
		title: "Are you sure you want to delete this record?",
		text: "You won't be able to revert this!",
		icon: "warning",
		showCancelButton: !0,
		confirmButtonColor: "#34c38f",
		cancelButtonColor: "#f46a6a",
		confirmButtonText: "Yes, delete it!",
	}).then(function (t) {
		// if user clickes yes, delete it.
		if (t.value) {
			$.ajax({
				url: apiURL + "program/" + id,
				type: "DELETE",
				dataType: "json",
				success: function (data) {
					if (data.success) {
						notification("success", "Success!", data.message);
						loadTable();
					} else {
						notification("error", "Error!", data.message);
					}
				},
				error: function ({ responseJSON }) {},
			});
		}
	});
};
